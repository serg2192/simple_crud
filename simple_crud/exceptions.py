class BaseError(Exception):
    DEFAULT_HTTP_STATUS = 500

    def __init__(
            self,
            code,
            message,
            details=None,
            http_status: int = None
    ):
        super().__init__()
        self._code = code
        self._message = message
        self._details = details
        self.http_status = http_status or self.DEFAULT_HTTP_STATUS

    @property
    def error_data(self):
        return {
            'code': self._code,
            'message': self._message,
            'details': self._details,
        }


class InvalidInputData(BaseError):
    HTTP_STATUS = 400

    def __init__(
            self,
            code=100,  # move it from here!!!
            message='Invalid data passed',  # move it from here!!!
            details=None
    ):
        super().__init__(
            code=code,
            message=message,
            details=details,
            http_status=self.HTTP_STATUS
        )
