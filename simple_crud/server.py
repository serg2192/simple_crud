import importlib
import logging
from aiohttp import web
import simple_crud.config
from simple_crud.resources.router import setup_routes


logger = logging.getLogger(__name__)

ON_STARTUP = (
    'simple_crud.services.tarantool:on_startup',
)

ON_CLEANUP = (
    'simple_crud.services.tarantool:on_cleanup',
)

MIDDLEWARE = (
    'simple_crud.middleware:base_response_middleware',
)


def imp(path):
    module, attr = path.split(':')
    module = importlib.import_module(module)
    return getattr(module, attr)


def _init_app():
    app = web.Application()
    app['config'] = simple_crud.config.create_config()
    for on_startup in ON_STARTUP:
        app.on_startup.append(imp(on_startup))
    for on_cleanup in ON_CLEANUP:
        app.on_cleanup.append(imp(on_cleanup))
    for middleware in MIDDLEWARE:
        app.middlewares.append(imp(middleware))
    setup_routes(app)
    for name, resource in app.router.named_resources().items():
        logger.info(f'{name} -- {resource}')
    return app


if __name__ == '__main__':
    app = _init_app()
    web.run_app(app, **app['config']['server'])
