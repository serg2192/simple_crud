import os
import pathlib
import yaml
import logging
import logging.config
from marshmallow import Schema, fields, INCLUDE


BASE_DIR = pathlib.Path(__file__).parent.parent
DEFAULT_CONF_PATH = os.path.join(
    BASE_DIR, 'config/config.sample.yaml'
)

logger = logging.getLogger(__name__)


class ConfigSchema(Schema):
    debug = fields.Boolean(missing=False)
    server = fields.Dict(missing={})  # nested may be better
    tarantool_cache = fields.Dict(missing={})  # nested may be better

    class Meta:
        unknown = INCLUDE


def create_config(path: str=None):
    config_path = os.getenv('CONFIG_PATH') or path or DEFAULT_CONF_PATH
    with open(config_path) as path:
        conf = yaml.safe_load(path)
    valid_config = ConfigSchema().load(conf)

    logging.config.dictConfig(valid_config['logging'])
    logger.info(
        f'Load config from: {config_path}\n'
        f'Loaded config: {valid_config}'
    )
    logger.debug(
        f"Loaded loggers:{list(logging.root.manager.loggerDict)}"
    )
    return valid_config
