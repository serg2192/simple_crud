from dataclasses import dataclass, asdict
import logging
import typing

logger = logging.getLogger(__name__)


@dataclass
class RequestInfo:
    method: str
    url: str
    params: typing.Optional[typing.Mapping]
    data: typing.Optional[typing.Mapping]
    json: typing.Optional[typing.Mapping]
    cookies: typing.Optional[typing.Mapping]
    headers: typing.Optional[typing.Mapping]

    def as_dict(self):
        return asdict(self)


def log_request(req):
    logging.info('THIS IS THE curl like request')
