from simple_crud.exceptions import BaseError


class TarantoolDatabaseError(BaseError):
    ERROR_CODE_TO_HTTP_STATUS_MAP = {
        3: 409,
    }

    @classmethod
    def from_error_code(cls, error_code, *args, **kwargs):
        http_status = cls.ERROR_CODE_TO_HTTP_STATUS_MAP.get(error_code, None)
        return cls(error_code, http_status=http_status, *args, **kwargs)
