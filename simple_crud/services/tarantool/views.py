from aiohttp.web import Request

from asynctnt import Connection
from asynctnt.exceptions import TarantoolDatabaseError

from . import base


async def base_crud_request(request: Request, method: str, **kwargs):
    """

    :param request: aiohttp.web.Request
    :param method: attribute to call
    :param kwargs: keyword params of request
    :return:
    """
    conn: Connection = request.app['tarantool_cache']
    method = getattr(conn, method)  # attribute error may occur
    try:
        result = await method(
            space='kv_store',
            **kwargs
        )
    except TarantoolDatabaseError as exc:
        raise base.TarantoolDatabaseError.from_error_code(
            error_code=exc.code,
            message=exc.message
        )
    return result


async def create(request: Request, key_value):  # keyvalue should be iterable or dict
    return await base_crud_request(
        request,
        method='insert',
        t=key_value,
    )


async def retrieve(request: Request, key):
    return await base_crud_request(
        request,
        method='select',
        key=key,
        # some useful features are skipped here
    )


async def update(request: Request, key):  # why not patch??
    return await base_crud_request(
        request,
        method='insert',
        key=key_value,
    )
    # return await base_crud_request(
    #     request,
    #     method='update',
    #     key=key,
    #     operations=...,  # check the spec
    #     # some useful features are skipped here
    # )


async def delete(request: Request, key):
    return await base_crud_request(
        request,
        method='delete',
        key=key,
        # some useful features are skipped here
    )
