import asynctnt


async def on_startup(app):
    connector = asynctnt.Connection(
        **app['config']['tarantool_cache']
    )
    await connector.connect()
    app['tarantool_cache'] = connector


async def on_cleanup(app):
    client: asynctnt.Connection = app['tarantool_cache']
    await client.disconnect()
