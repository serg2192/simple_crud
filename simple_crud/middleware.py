import logging
from aiohttp.web import middleware, json_response
from webargs.aiohttpparser import AIOHTTPParser
from simple_crud.generics import log_request
from simple_crud.exceptions import BaseError, InvalidInputData

logger = logging.getLogger(__name__)


class Parser(AIOHTTPParser):

    async def parse(
            self,
            *args,
            **kwargs
    ):
        log_request(kwargs['req'])
        parsed_data = await super().parse(*args, **kwargs)
        return parsed_data

    async def handle_error(
        self,
        error,
        *args, **kwargs
    ):
        raise InvalidInputData(details=str(error))
        # raise BadRequest(details=error) raise a code error with 400 status code

    async def load_files(self, *args, **kwargs):
        raise NotImplementedError


parser = Parser()
use_args = parser.use_args


@middleware
async def base_response_middleware(request, handler):
    try:
        status = 200
        response = {
            'data': await handler(request),
            'metadata': {
                'code': 0,
                'message': 'OK'
            }
        }
    except BaseError as exc:
        status = exc.http_status
        response = {  # recreate metadata based on specific route and exception!!!
            'data': {},
            # 'metadata': {
            #     'code': 0,
            #     'message': 'OK'
            # }
            'metadata': exc.error_data
        }
    except Exception as exc:
        logger.error(f'Failed with exception: {exc}', exc_info=True)
        raise
    return json_response(
        data=response,
        status=status
    )
