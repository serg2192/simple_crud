from aiohttp.web import Application
from aiohttp import web
from simple_crud.resources.tarantool_cache import views as tc_views


def setup_routes(app: Application) -> None:
    app.router.add_routes(
        [
            web.route(
                'POST',
                '/cache/create',
                tc_views.create,
                name='cache_create'
            ),
        ]
    )
