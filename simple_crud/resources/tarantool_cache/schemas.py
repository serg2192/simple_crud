import typing
import json
from marshmallow import Schema, fields, pre_load


class JSONField(fields.Field):

    def _serialize(
            self,
            value: typing.Any,
            attr: str,
            obj: typing.Any,
            **kwargs
    ):
        return json.loads(value)

    def deserialize(
        self,
        value: typing.Any,
        attr: str = None,
        data: typing.Mapping[str, typing.Any] = None,
        **kwargs
    ):
        return json.dumps(value)


class CreateCacheRecordSchema(Schema):
    # key = fields.String()
    key = JSONField()
    value = JSONField()  # may be not the best idea

    class Meta:
        strict = True

    # @pre_load
    # def key_as_a_string(self, data, many, **kwargs):
    #     data['key'] = str(data['key'])  # ugly shit
    #     return data
