from aiohttp.web import Request

from simple_crud.middleware import use_args
from simple_crud.resources.tarantool_cache import schemas

from simple_crud.services.tarantool import views as t_views


@use_args(schemas.CreateCacheRecordSchema(), location='json')
async def create(request: Request, args):
    res = await t_views.create(request, (args['key'], args['value']))
    return dict(res.body)  # may be not the best idea
