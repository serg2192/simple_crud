*На данный момент реализован метод создания записи в кеше*

1. Успешное создание записи
    curl -w '\n' -iX POST 'http://127.0.0.1:8000/cache/create' -H 'Content-Type:application/json' -d '{"key":1300, "value":{"x":100500}}'
    HTTP/1.1 200 OK
    Content-Type: application/json; charset=utf-8
    Content-Length: 79
    Date: Wed, 04 Mar 2020 22:44:56 GMT
    Server: Python/3.7 aiohttp/3.6.2

    {"data": {"1300": "{\"x\": 100500}"}, "metadata": {"code": 0, "message": "OK"}}
1. Запись уже существует
    curl -w '\n' -iX POST 'http://127.0.0.1:8000/cache/create' -H 'Content-Type:application/json' -d '{"key":1300, "value":{"x":100500}}'
    HTTP/1.1 409 Conflict
    Content-Type: application/json; charset=utf-8
    Content-Length: 135
    Date: Wed, 04 Mar 2020 22:43:34 GMT
    Server: Python/3.7 aiohttp/3.6.2
    {"data": {}, "metadata": {"code": 3, "message": "Duplicate key exists in unique index 'primary' in space 'kv_store'", "details": null}}
    
1. Невалидное body (спеку надо привести к общему виду)
    curl -w '\n' -iX POST 'http://127.0.0.1:8000/cache/create' -H 'Content-Type:application/json' -d '{"key":1300, "value":}'
    HTTP/1.1 400 Bad Request
    Content-Type: application/json
    Content-Length: 32
    Date: Wed, 04 Mar 2020 22:49:33 GMT
    Server: Python/3.7 aiohttp/3.6.2
    
    {"json": ["Invalid JSON body."]}

