-- настройки типа listen скорее всего надо из env брать
box.cfg{
	listen = 3301
}

box.once('schema', 
	function()
		box.schema.create_space('kv_store',
			{ 
				format = {
					{ name = 'key';   type = 'string' },
					{ name = 'value'; type = '*' },
				};
				if_not_exists = true;
			}
		)
		box.space.kv_store:create_index('primary', 
			{ type = 'hash'; parts = {1, 'string'}; if_not_exists = true; unique=true}
		)
	end
)
